"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const _ = require("lodash");
const operators_1 = require("rxjs/operators");
const rxjs_1 = require("rxjs");
const notify_1 = require("./tools/notify");
const organization_user_factory_1 = require("./tools/organization-user.factory");
const check_user_phone_function_1 = require("./tools/check-user-phone.function");
exports.onInviteUser = async (req, res) => {
    const db = admin.firestore();
    const user = req.body.data;
    console.log("Running onInviteUser function...");
    console.log("[onInviteUser function]-request:", req);
    console.log("[onInviteUser function]-user:", user);
    const createAndInvite$ = () => rxjs_1.from(db.collection('invited_user')
        .add(_.omit(Object.assign({}, user, { invited: true }), ['organization', 'id'])))
        .pipe(operators_1.switchMap(doc => db.collection('organization_user')
        .add(organization_user_factory_1.buildOrganizationUser(doc.id, user, true))), operators_1.tap(() => notify_1.notify(user, 'createAndInvite')));
    const invite$ = (userId, invited) => rxjs_1.from(db.collection('organization_user')
        .add(organization_user_factory_1.buildOrganizationUser(userId, user, invited)))
        .pipe(operators_1.tap(() => notify_1.notify(user, 'invite')));
    check_user_phone_function_1.checkUserPhone(user.phoneNumber, user.organization.id)
        .pipe(operators_1.switchMap(result => {
        console.log("[onInviteUser function]-checkUserPhone-result:", result);
        if (!result || !result.organization) {
            return !result ?
                createAndInvite$() : invite$(result.user.id, !!result.user.invited);
        }
        else {
            return rxjs_1.of(true);
        }
    }), operators_1.tap(tapResult => console.log("[onInviteUser function]-checkUserPhone-tap-result:", tapResult)))
        .subscribe(doc => {
        console.log("[onInviteUser function]-created-doc:", doc);
    }, error => {
        console.log("[onInviteUser function]-error:", error);
    });
};
//# sourceMappingURL=invite-user.function.js.map