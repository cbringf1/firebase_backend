"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const AWS = require("aws-sdk");
const _ = require("lodash");
class SMSNotification {
    constructor() {
        AWS.config.credentials = _.merge(AWS.config.credentials, _.omit(_.mapKeys(functions.config().aws_sms, (v, k) => _.camelCase(k)), 'region'));
        AWS.config.update(_.pick(functions.config().aws_sms, ['region']));
        this.awsSNS = new AWS.SNS({ apiVersion: '2010-03-31' });
    }
    send(phoneNumber, message) {
        return this.awsSNS.publish({
            PhoneNumber: phoneNumber,
            Message: message
        }).promise();
    }
}
exports.SMSNotification = SMSNotification;
//# sourceMappingURL=sms-notification.js.map