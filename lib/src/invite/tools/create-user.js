"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const update_invitation_1 = require("./update-invitation");
exports.createUser = (user, docId, invitedUserDocId) => {
    const db = admin.firestore();
    db.collection('user').doc(docId).set({
        firstName: user.firstName,
        lastName: user.lastName,
        phoneNumber: user.phoneNumber,
        isActive: true,
        address: null,
        call911: true,
        email: null
    }).then(() => {
        update_invitation_1.updateOrganizationUserAfterUserLogIn(docId, invitedUserDocId);
        console.log('New user created:', user);
    }).catch(error => console.log(error));
};
//# sourceMappingURL=create-user.js.map