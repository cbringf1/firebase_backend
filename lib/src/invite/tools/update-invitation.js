"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
exports.updateOrganizationUserAfterUserLogIn = (docId, invitedUserDocId) => {
    const db = admin.firestore();
    db.collection('organization_user')
        .where('userId', '==', invitedUserDocId)
        .get()
        .then(query => {
        query.docs.forEach(doc => {
            db.collection('organization_user')
                .doc(doc.id)
                .update({
                invitedUser: false,
                userId: docId
            }).then(() => console.log('organization_user collection updated', doc.id))
                .catch(error => console.log(error));
        });
    }).catch(error => console.log(error));
};
//# sourceMappingURL=update-invitation.js.map