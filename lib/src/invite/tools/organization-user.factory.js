"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildOrganizationUser = (userDocId, invitedUser, newUser = false) => {
    let organizationUser = {
        organizationId: invitedUser.organization.id,
        organizationName: invitedUser.organization.name,
        userId: userDocId,
        userName: `${invitedUser.firstName} ${invitedUser.lastName}`,
        status: 'pending user',
        role: 'admin',
        invitedUser: newUser,
        userPhoneNumber: invitedUser.phoneNumber.replace(/\s/g, '')
    };
    console.log("Running buildOrganizationUser function...");
    console.log("[buildOrganizationUser function]-userDocId:", userDocId);
    console.log("[buildOrganizationUser function]-invitedUser:", invitedUser);
    console.log("[buildOrganizationUser function]-organizationUser:", organizationUser);
    return organizationUser;
};
//# sourceMappingURL=organization-user.factory.js.map