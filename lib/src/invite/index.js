"use strict";
// Cloud functions related to users invitations management
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const format_phone_function_1 = require("./format-phone.function");
const signin_invited_user_function_1 = require("./signin-invited-user.function");
const awake_1 = require("../awake");
// Collection triggers
exports.createdAtTrigger = functions.firestore
    .document('organization_user/{orgId}')
    .onCreate((snap) => snap.ref.set(Object.assign({}, snap.data(), { createdAt: snap.createTime })));
exports.formatPhoneTrigger = functions.firestore
    .document('invited_user/{userId}')
    .onCreate((snap, context) => awake_1.warmEventFilter(format_phone_function_1.onFormatPhone, snap, context));
exports.onUpdateInvitedUserTrigger = functions.firestore
    .document('invited_user/{id}')
    .onUpdate((change, context) => awake_1.warmEventFilter(signin_invited_user_function_1.onSignInInvitedUser, change, context));
//# sourceMappingURL=index.js.map