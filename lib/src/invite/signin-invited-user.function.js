"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const create_user_1 = require("./tools/create-user");
exports.onSignInInvitedUser = (change, context) => {
    const docRef = change.after;
    const invitedUser = docRef.data();
    if (invitedUser) {
        const user = {
            firstName: invitedUser.firstName,
            lastName: invitedUser.lastName,
            phoneNumber: invitedUser.phoneNumber,
            isActive: true
        };
        create_user_1.createUser(user, invitedUser.id, docRef.id);
    }
    else {
        console.log(config_1.FunctionErrors.emptyUser.title, config_1.FunctionErrors.emptyUser.msg);
        console.log('Context:', context);
    }
};
//# sourceMappingURL=signin-invited-user.function.js.map