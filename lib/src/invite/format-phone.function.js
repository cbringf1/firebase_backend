"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onFormatPhone = (snap) => {
    const phoneNumber = (snap.data() || { phoneNumber: '' }).phoneNumber.replace(/\s/g, '');
    return snap.ref.set(Object.assign({}, snap.data(), { phoneNumber }));
};
//# sourceMappingURL=format-phone.function.js.map