"use strict";
// Functions to redirect users to correct app store [Android, IOS]
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const open_function_1 = require("./open.function");
exports.openStore = functions.https.onRequest(open_function_1.onOpenStore);
//# sourceMappingURL=index.js.map