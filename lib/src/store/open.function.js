"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PLAY_STORE = 'https://play.google.com/store/apps/details?id=com.fusus.fususalert&hl=en_US';
const APP_STORE = 'https://apps.apple.com/us/app/fususalert/id1475659632';
const FUSUS = 'https://fusus.com';
exports.onOpenStore = (req, res) => {
    const userAgent = req.headers["user-agent"];
    if (/android/i.test(userAgent)) {
        res.redirect(PLAY_STORE);
    }
    else if (/iPad|iPhone|iPod/.test(userAgent)) {
        res.redirect(APP_STORE);
    }
    else {
        res.redirect(FUSUS);
    }
};
//# sourceMappingURL=open.function.js.map