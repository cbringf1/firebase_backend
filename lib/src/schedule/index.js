"use strict";
// Scheduled functions
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const awake_1 = require("../awake");
exports.scheduledFunction = functions.pubsub.schedule('every 5 minutes')
    .onRun(() => awake_1.awake());
//# sourceMappingURL=index.js.map