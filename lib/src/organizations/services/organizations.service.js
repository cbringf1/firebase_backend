"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_enum_1 = require("../../core/enums/http-status.enum");
const prepare_document_data_1 = require("../../core/helpers/prepare-document-data");
const organizations_db_1 = require("../db/organizations.db");
class OrganizationsService {
    static async get(id) {
        const organizationDocs = await organizations_db_1.ORGANIZATIONS
            .where('id', '==', id)
            .get();
        if (organizationDocs.empty) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `Organization with id ${id} not found`,
            };
        }
        return prepare_document_data_1.prepareDocumentData(organizationDocs.docs[0]);
    }
}
exports.OrganizationsService = OrganizationsService;
//# sourceMappingURL=organizations.service.js.map