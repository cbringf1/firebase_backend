"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const invite_user_function_1 = require("./invite/invite-user.function");
const on_awake_1 = require("./awake/on-awake");
const routes = {
    'inviteUser': invite_user_function_1.onInviteUser,
    'awake': on_awake_1.onAwake
};
exports.router = (req, res) => {
    const fn = req.params.fnName;
    if (routes[fn]) {
        routes[fn](req, res);
    }
    else {
        res.status(404).send('No function');
    }
};
//# sourceMappingURL=router.js.map