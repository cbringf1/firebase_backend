"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid = require("uuid");
const environment_1 = require("../../environments/environment");
const create_alarm_api_1 = require("./tools/create-alarm.api");
const config_1 = require("../config");
const users_service_1 = require("../auth/services/users.service");
const create_alarm_tool_fuctions_1 = require("./tools/create-alarm.tool.fuctions");
exports.onCreateAlarm = async (snap, context) => {
    const alarm = snap.data();
    if (alarm) {
        let onCreateAlarmData = {
            type: environment_1.environment.type,
            alias: uuid().slice(0, 8),
            geometry: { type: "Point", coordinates: [alarm.longitude, alarm.latitude] },
            organization: alarm.organizationId,
            details: {
                "User Name": alarm.user.firstName + ' ' + alarm.user.lastName,
                "User Phone Number": alarm.user.phoneNumber
            },
            // ...(environment.recording_url) && { recording_url: environment.recording_url },
            // ...(environment.source) && {
            //     source: {
            //         object_id: environment.source.object_id,
            //         object_type: environment.source.object_type
            //     }
            // },
            message: environment_1.environment.message,
        };
        // const organizationDoc = await getOrganizationById(alarm.organizationId).catch(() => undefined);
        // const organizationId = organizationDoc ? organizationDoc.id : null
        const userDoc = await users_service_1.UsersService.getByPhone(alarm.user.phoneNumber).catch(() => undefined);
        const userId = userDoc ? userDoc.id : null;
        // console.log(`User id: ${userId} - Organization Id: ${organizationId}`);
        if (userId && alarm.organizationId) {
            const organizationUserDoc = await create_alarm_tool_fuctions_1.getAlarmExtraInfo(alarm.organizationId, userId);
            if (organizationUserDoc) {
                onCreateAlarmData = Object.assign({}, onCreateAlarmData, (organizationUserDoc.recording_url) && {
                    recording_url: organizationUserDoc.recording_url
                }, (organizationUserDoc.source) && {
                    source: {
                        object_id: organizationUserDoc.source.object_id,
                        object_type: organizationUserDoc.source.object_type
                    }
                });
                const alarmData = JSON.stringify(onCreateAlarmData);
                create_alarm_api_1.onCreateAlarmApiCall(alarmData, environment_1.environment.apiUrl, environment_1.environment.authToken);
                return 'OK';
            }
        }
        else {
            console.log(config_1.FunctionErrors.emptyAlarm.title, config_1.FunctionErrors.emptyAlarm.msg);
            console.log('Context:', context);
        }
    }
    return 'Fail';
};
//# sourceMappingURL=create-alarm.function.js.map