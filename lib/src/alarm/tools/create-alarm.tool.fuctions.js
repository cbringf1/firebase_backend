"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const alert_user_invites_db_1 = require("../db/alert-user-invites.db");
const prepare_document_data_1 = require("../../core/helpers/prepare-document-data");
const http_status_enum_1 = require("../../core/enums/http-status.enum");
// import { ORGANIZATIONS } from "../../organizations/db/organizations.db";
exports.getAlarmExtraInfo = async (orgId, userId) => {
    const organizationUserDoc = await alert_user_invites_db_1.ORGANIZATION_USER
        .where('organizationId', '==', orgId)
        .where('userId', '==', userId)
        .get();
    if (organizationUserDoc.empty) {
        throw {
            status: http_status_enum_1.HttpStatus.NOT_FOUND,
            error: 'Not Found',
            message: `Organization for user with the Id: ${userId} not found`,
        };
    }
    return prepare_document_data_1.prepareDocumentData(organizationUserDoc.docs[0]);
};
// export const getOrganizationById = async (id: string) => {
//     const orgDocs = await ORGANIZATIONS
//         .where('id', '==', id)
//         .get();
//     if (orgDocs.empty) {
//         throw {
//             status: HttpStatus.NOT_FOUND,
//             error: 'Not Found',
//             message: `Organization with the id ${id} not found`,
//         };
//     }
//     return prepareDocumentData(orgDocs.docs[0]);
// }
//# sourceMappingURL=create-alarm.tool.fuctions.js.map