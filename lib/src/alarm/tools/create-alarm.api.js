"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const xmlhttprequest_ts_1 = require("xmlhttprequest-ts");
exports.onCreateAlarmApiCall = (jsonData, apiUrl, auth) => {
    const http = new xmlhttprequest_ts_1.XMLHttpRequest();
    http.onreadystatechange = () => {
        console.log('ResponseText:', http.responseText);
    };
    http.open('POST', apiUrl);
    http.setRequestHeader('Authorization', auth);
    http.setRequestHeader('Content-Type', 'application/json');
    http.setRequestHeader('Accept', 'application/json');
    http.send(jsonData);
};
//# sourceMappingURL=create-alarm.api.js.map