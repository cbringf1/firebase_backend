"use strict";
// Cloud functions related to alarm creation
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const awake_1 = require("../awake");
const create_alarm_function_1 = require("./create-alarm.function");
exports.onCreateAlarmTrigger = functions.firestore
    .document('alarm/{userId}')
    .onCreate((snap, context) => awake_1.warmEventFilter(create_alarm_function_1.onCreateAlarm, snap, context));
//# sourceMappingURL=index.js.map