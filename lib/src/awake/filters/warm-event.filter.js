"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const warmDocKey = '_warming';
exports.warmEventFilter = (fn, target, context) => {
    let data = null;
    if (target instanceof admin.firestore.DocumentSnapshot) {
        data = target.data();
    }
    else {
        data = target.after.data();
    }
    return data && !(warmDocKey in data)
        ? fn(target, context)
        : Promise.resolve('warming');
};
//# sourceMappingURL=warm-event.filter.js.map