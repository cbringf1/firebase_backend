"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios = require("axios");
exports.awake = () => {
    return axios.default.get('https://us-central1-fieldwatch-dev.cloudfunctions.net/api/awake')
        .then(() => 'ok')
        .catch(err => console.error(err));
};
//# sourceMappingURL=request.js.map