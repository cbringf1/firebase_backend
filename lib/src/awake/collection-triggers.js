"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const collections = [
    'alarm',
    'invited_user',
];
const warmDocKey = '_warming';
exports.awakeCollectionsTriggers = async () => {
    for (const collection of collections) {
        const docRef = await admin
            .firestore()
            .collection(collection)
            .add({
            [warmDocKey]: true,
        });
        await docRef.update({
            [warmDocKey]: false,
        });
        await docRef.delete();
    }
};
//# sourceMappingURL=collection-triggers.js.map