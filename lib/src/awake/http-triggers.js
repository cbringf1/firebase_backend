"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios = require("axios");
exports.awakeHttpTriggers = () => {
    console.log("Running awakeHttpTriggers function...");
    return axios.default.get('https://us-central1-fieldwatch-dev.cloudfunctions.net/api/awake');
};
//# sourceMappingURL=http-triggers.js.map