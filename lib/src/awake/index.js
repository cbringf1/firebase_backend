"use strict";
// Functions to keep warm firebase instances
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const collection_triggers_1 = require("./collection-triggers");
__export(require("./filters"));
exports.awake = async () => {
    await collection_triggers_1.awakeCollectionsTriggers();
    return true;
};
//# sourceMappingURL=index.js.map