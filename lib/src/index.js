"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
admin.initializeApp();
__export(require("./api/api.functions"));
__export(require("./auth/auth.functions"));
__export(require("./alert-users/alert-users.functions"));
__export(require("./schedule"));
__export(require("./alarm"));
__export(require("./store"));
//# sourceMappingURL=index.js.map