"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const xmlhttprequest_ts_1 = require("xmlhttprequest-ts");
exports.onCreateAlarmApiCall = (jsonData, apiUrl, auth) => {
    const http = new xmlhttprequest_ts_1.XMLHttpRequest();
    http.onreadystatechange = () => {
        console.log('ResponseText:', http.responseText);
    };
    http.open('POST', apiUrl);
    http.setRequestHeader('Authorization', auth);
    http.setRequestHeader('Content-Type', 'application/json');
    http.setRequestHeader('Accept', 'application/json');
    http.send(jsonData);
};
exports.createUser = (user, docId, invitedUserDocId) => {
    const db = admin.firestore();
    db.collection('user').doc(docId).set({
        firstName: user.firstName,
        lastName: user.lastName,
        phoneNumber: user.phoneNumber,
        isActive: true,
        address: null,
        call911: true,
        email: null
    }).then(() => {
        exports.updateOrganizationUserAfterUserLogIn(docId, invitedUserDocId);
        console.log('New user created:', user);
    }).catch(error => console.log(error));
};
exports.updateOrganizationUserAfterUserLogIn = (docId, invitedUserDocId) => {
    const db = admin.firestore();
    db.collection('organization_user')
        .where('userId', '==', invitedUserDocId)
        .get()
        .then(query => {
        query.docs.forEach(doc => {
            db.collection('organization_user')
                .doc(doc.id)
                .update({
                invitedUser: false,
                userId: docId
            }).then(() => console.log('organization_user collection updated', doc.id))
                .catch(error => console.log(error));
        });
    }).catch(error => console.log(error));
};
//# sourceMappingURL=functions.js.map