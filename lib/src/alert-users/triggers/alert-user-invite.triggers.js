"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
exports.onAlertUserInviteCreate = functions.firestore
    .document('organization_user/{id}')
    .onCreate((snap) => {
    return snap.ref.update({
        createdAt: snap.createTime,
    });
});
//# sourceMappingURL=alert-user-invite.triggers.js.map