"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const alert_user_invites_db_1 = require("../db/alert-user-invites.db");
const alert_user_invites_service_1 = require("../services/alert-user-invites.service");
exports.onInvitedUserUpdate = functions.firestore
    .document('invited_user/{id}')
    .onUpdate(async (change) => {
    const snapshot = change.after;
    const data = snapshot.data();
    const userId = data && data.id;
    if (userId) {
        const userInvites = await alert_user_invites_db_1.ALERT_USER_INVITES
            .where('userId', '==', snapshot.id)
            .get();
        for (const invite of userInvites.docs) {
            await alert_user_invites_service_1.AlertUserInvitesService.update(invite.id, { userId });
        }
    }
});
//# sourceMappingURL=invited-users.triggers.js.map