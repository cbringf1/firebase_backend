"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const list_1 = require("../../core/helpers/list");
const http_status_enum_1 = require("../../core/enums/http-status.enum");
const alert_user_invites_db_1 = require("../db/alert-user-invites.db");
const alert_user_invite_status_enum_1 = require("../enums/alert-user-invite-status.enum");
const alert_user_invites_service_1 = require("../services/alert-user-invites.service");
class AlertUserInvitesController {
    static async list(req, res, next) {
        const session = req.session;
        const collection = await alert_user_invites_db_1.ALERT_USER_INVITES
            .where('organizationId', '==', session.organizationId);
        const response = await list_1.list(collection, req.query);
        return res.status(200).send(response);
    }
    static async create(req, res, next) {
        const session = req.session;
        try {
            const invite = await alert_user_invites_service_1.AlertUserInvitesService.create(req.body, session.organizationId);
            return res.status(http_status_enum_1.HttpStatus.CREATED).send(invite);
        }
        catch (e) {
            return next(e);
        }
    }
    static async activate(req, res, next) {
        try {
            const invite = await alert_user_invites_service_1.AlertUserInvitesService.update(req.params.id, {
                status: alert_user_invite_status_enum_1.AlertUserIntiveStatus.AcceptedOrganization,
            });
            return res.status(http_status_enum_1.HttpStatus.OK).send(invite);
        }
        catch (e) {
            return next(e);
        }
    }
    static async deactivate(req, res, next) {
        try {
            const invite = await alert_user_invites_service_1.AlertUserInvitesService.update(req.params.id, {
                status: alert_user_invite_status_enum_1.AlertUserIntiveStatus.AcceptedOrganization,
            });
            return res.status(http_status_enum_1.HttpStatus.OK).send(invite);
        }
        catch (e) {
            return next(e);
        }
    }
}
exports.AlertUserInvitesController = AlertUserInvitesController;
//# sourceMappingURL=alert-user-invites.controller.js.map