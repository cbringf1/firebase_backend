"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const normalize_phone_sanitizer_1 = require("../../core/helpers/normalize-phone.sanitizer");
const alert_user_invites_db_1 = require("../db/alert-user-invites.db");
async function phoneNumberIsUnique(value, { req }) {
    const organizationId = req.session.organizationId;
    const inviteIsExisted = await alert_user_invites_db_1.ALERT_USER_INVITES
        .where('organizationId', '==', organizationId)
        .where('userPhoneNumber', '==', value)
        .get();
    if (!inviteIsExisted.empty) {
        throw new Error('Phone number already exists');
    }
    return inviteIsExisted.empty;
}
exports.inviteCreateValidators = [
    express_validator_1.sanitizeBody('phoneNumber').customSanitizer(normalize_phone_sanitizer_1.normalizePhoneSanitizer),
    express_validator_1.body('firstName')
        .exists()
        .withMessage('Field is required'),
    express_validator_1.body('lastName')
        .exists()
        .withMessage('Field is required'),
    express_validator_1.body('phoneNumber')
        .exists()
        .withMessage('Field is required')
        .custom(phoneNumberIsUnique)
        .withMessage('Phone number already exists'),
];
//# sourceMappingURL=invite-create.validators.js.map