"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sms_notification_1 = require("../../core/services/sms-notification");
const http_status_enum_1 = require("../../core/enums/http-status.enum");
const users_service_1 = require("../../auth/services/users.service");
const prepare_document_data_1 = require("../../core/helpers/prepare-document-data");
const organizations_service_1 = require("../../organizations/services/organizations.service");
const alert_user_invites_db_1 = require("../db/alert-user-invites.db");
const alert_user_invite_status_enum_1 = require("../enums/alert-user-invite-status.enum");
const invited_users_service_1 = require("./invited-users.service");
class AlertUserInvitesService {
    static async create(userData, organizationId) {
        const user = await users_service_1.UsersService.getByPhone(userData.phoneNumber).catch(() => undefined);
        const organization = await organizations_service_1.OrganizationsService
            .get(organizationId)
            .catch((error) => {
            throw {
                status: http_status_enum_1.HttpStatus.BAD_REQUEST,
                error: 'Bad Request',
                message: error.message,
            };
        });
        try {
            const inviteUser = await invited_users_service_1.InvitedUsersService
                .getByPhone(userData.phoneNumber)
                .catch(() => invited_users_service_1.InvitedUsersService.create(userData));
            const inviteData = {
                userId: user ? user.id : inviteUser.id,
                userName: `${userData.firstName} ${userData.lastName}`,
                userPhoneNumber: userData.phoneNumber,
                organizationId,
                organizationName: organization.name,
                status: alert_user_invite_status_enum_1.AlertUserIntiveStatus.PendingUser,
            };
            const inviteRef = await alert_user_invites_db_1.ALERT_USER_INVITES.add(inviteData);
            const inviteDoc = await inviteRef.get();
            await sms_notification_1.SMS.send(userData.phoneNumber, `${organization.name} have invited you to join the organization in fusus Panic App. http://alert.fususone.com/`);
            return prepare_document_data_1.prepareDocumentData(inviteDoc);
        }
        catch (error) {
            throw {
                status: http_status_enum_1.HttpStatus.CONFLICT,
                error: 'Conflict',
                message: 'Could not create alert user invite',
            };
        }
    }
    static async get(id) {
        const inviteDoc = await alert_user_invites_db_1.ALERT_USER_INVITES.doc(id).get();
        if (!inviteDoc.exists) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `Alert user invite with id ${id} not found`,
            };
        }
        return prepare_document_data_1.prepareDocumentData(inviteDoc);
    }
    static async update(id, data) {
        const inviteDoc = await alert_user_invites_db_1.ALERT_USER_INVITES.doc(id).get();
        if (!inviteDoc.exists) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `Alert user invite with id ${id} not found`,
            };
        }
        try {
            await inviteDoc.ref.update(data);
            return await AlertUserInvitesService.get(id);
        }
        catch (e) {
            throw {
                status: http_status_enum_1.HttpStatus.CONFLICT,
                error: 'Conflict',
                message: 'Could not update alert user invite',
            };
        }
    }
}
exports.AlertUserInvitesService = AlertUserInvitesService;
//# sourceMappingURL=alert-user-invites.service.js.map