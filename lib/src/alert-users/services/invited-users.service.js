"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_enum_1 = require("../../core/enums/http-status.enum");
const prepare_document_data_1 = require("../../core/helpers/prepare-document-data");
const invited_users_db_1 = require("../db/invited-users.db");
class InvitedUsersService {
    static async create(data) {
        try {
            const userRef = await invited_users_db_1.INVITED_USERS.add(data);
            const userDoc = await userRef.get();
            return prepare_document_data_1.prepareDocumentData(userDoc);
        }
        catch (e) {
            throw {
                status: http_status_enum_1.HttpStatus.CONFLICT,
                error: 'Conflict',
                message: 'Could not create invited user',
            };
        }
    }
    static async get(id) {
        const userDoc = await invited_users_db_1.INVITED_USERS.doc(id).get();
        if (!userDoc.exists) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `Invited user with id ${id} not found`,
            };
        }
        return prepare_document_data_1.prepareDocumentData(userDoc);
    }
    static async getByPhone(phone) {
        const userDocs = await invited_users_db_1.INVITED_USERS
            .where('phoneNumber', '==', phone)
            .get();
        if (userDocs.empty) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `Alert user with phone ${phone} not found`,
            };
        }
        return prepare_document_data_1.prepareDocumentData(userDocs.docs[0]);
    }
}
exports.InvitedUsersService = InvitedUsersService;
//# sourceMappingURL=invited-users.service.js.map