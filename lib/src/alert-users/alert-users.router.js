"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const fusus_user_guard_1 = require("../auth/guards/fusus-user.guard");
const authorized_guard_1 = require("../auth/guards/authorized.guard");
const validation_handler_1 = require("../core/handlers/validation.handler");
const invite_create_validators_1 = require("./validators/invite-create.validators");
const alert_user_invites_controller_1 = require("./controllers/alert-user-invites.controller");
const router = express.Router();
function AlertUsersRouter(appRouter) {
    appRouter.use('/alert-users', router);
    router.use(authorized_guard_1.authorizedGuard, fusus_user_guard_1.fususUserGuard);
    router.get('/invites', alert_user_invites_controller_1.AlertUserInvitesController.list);
    router.post('/invites', invite_create_validators_1.inviteCreateValidators, validation_handler_1.validationHandler, alert_user_invites_controller_1.AlertUserInvitesController.create);
    router.patch('/invites/:id/activate', alert_user_invites_controller_1.AlertUserInvitesController.activate);
    router.patch('/invites/:id/deactivate', alert_user_invites_controller_1.AlertUserInvitesController.deactivate);
}
exports.AlertUsersRouter = AlertUsersRouter;
//# sourceMappingURL=alert-users.router.js.map