"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AlertUserIntiveStatus;
(function (AlertUserIntiveStatus) {
    AlertUserIntiveStatus["PendingUser"] = "pending user";
    AlertUserIntiveStatus["PendingOrganization"] = "pending organization";
    AlertUserIntiveStatus["RejectedUser"] = "rejected user";
    AlertUserIntiveStatus["RejectedOrganization"] = "rejected organization";
    AlertUserIntiveStatus["AcceptedUser"] = "accepted user";
    AlertUserIntiveStatus["AcceptedOrganization"] = "accepted organization";
})(AlertUserIntiveStatus = exports.AlertUserIntiveStatus || (exports.AlertUserIntiveStatus = {}));
//# sourceMappingURL=alert-user-invite-status.enum.js.map