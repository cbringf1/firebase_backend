"use strict";
// Configuration of api route /api/[cloud-functions]
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");
const router_1 = require("./router");
const app = express();
app.use(cors({ origin: true }));
app.get('/:fnName', router_1.router);
app.post('/:fnName', router_1.router);
exports.api = functions.https.onRequest(app);
//# sourceMappingURL=index.js.map