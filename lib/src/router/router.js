"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const invite_user_function_1 = require("../invite/invite-user.function");
const auth_1 = require("../auth");
const routes = {
    'inviteUser': invite_user_function_1.onInviteUser
};
exports.router = (req, res) => {
    const fn = req.params.fnName;
    console.log("Running router function...");
    console.log("[router function]-fn:", fn);
    console.log("[router function]-routes[fn]:", routes[fn]);
    if (routes[fn]) {
        auth_1.validateFirebaseToken(req, res, routes[fn](req, res))
            .catch(error => console.log("[router function]-error:", error));
    }
    else if (fn === 'awake') {
        res.send('Awake');
    }
    else {
        res.status(404).send('No function');
    }
};
//# sourceMappingURL=router.js.map