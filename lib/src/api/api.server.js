"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const error_handler_1 = require("../core/handlers/error.handler");
const api_router_1 = require("./api.router");
const server = express();
server.use(cors({
    origin: true,
    optionsSuccessStatus: 200,
}));
server.use(express.json());
server.use('/api', api_router_1.ApiRouter());
server.use(error_handler_1.errorHandler);
exports.ApiServer = server;
//# sourceMappingURL=api.server.js.map