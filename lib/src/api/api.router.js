"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const auth_router_1 = require("../auth/auth.router");
const alert_users_router_1 = require("../alert-users/alert-users.router");
const schema_action_1 = require("./actions/schema.action");
function ApiRouter() {
    const router = express.Router();
    auth_router_1.AuthRouter(router);
    alert_users_router_1.AlertUsersRouter(router);
    router.get('/schema', schema_action_1.schemaAction(router));
    return router;
}
exports.ApiRouter = ApiRouter;
//# sourceMappingURL=api.router.js.map