"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function schemaAction(router) {
    const routes = router.stack.reduce((acc, stack) => {
        if (stack.route) {
            acc.push(stack.route);
        }
        else if (stack.name === 'router') {
            stack.handle.stack
                .filter((handler) => !!handler.route)
                .forEach((handler) => {
                acc.push(handler);
            });
        }
        return acc;
    }, []);
    return (req, res) => {
        res.status(200).send(routes);
    };
}
exports.schemaAction = schemaAction;
//# sourceMappingURL=schema.action.js.map