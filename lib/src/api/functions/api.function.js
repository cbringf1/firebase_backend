"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const api_server_1 = require("../api.server");
exports.api = functions.https.onRequest(api_server_1.ApiServer);
//# sourceMappingURL=api.function.js.map