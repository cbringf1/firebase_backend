"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const users_service_1 = require("../services/users.service");
exports.onAuthUserCreate = functions.auth
    .user()
    .onCreate(async (user) => {
    await users_service_1.UsersService.create(user.uid, {
        email: user.email,
        phoneNumber: user.phoneNumber,
    });
});
exports.onAuthUserDelete = functions.auth
    .user()
    .onDelete(async (user) => {
    await users_service_1.UsersService.delete(user.uid);
});
//# sourceMappingURL=auth.triggers.js.map