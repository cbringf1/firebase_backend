"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Session {
    constructor(decodedToken) {
        this.userId = decodedToken.sub;
        this.organizationId = decodedToken.organization_id;
        this.provider = decodedToken.firebase.sign_in_provider;
        this.isFususUser = this.provider === 'custom' && !!this.organizationId;
        this.isFirebaseUser = !this.isFususUser;
    }
}
exports.Session = Session;
//# sourceMappingURL=session.js.map