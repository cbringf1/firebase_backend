"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const authorized_guard_1 = require("./guards/authorized.guard");
const current_user_action_1 = require("./actions/current-user.action");
const router = express.Router();
function AuthRouter(appRouter) {
    appRouter.use(router);
    router.get('/current-user', authorized_guard_1.authorizedGuard, current_user_action_1.currentUserAction);
}
exports.AuthRouter = AuthRouter;
//# sourceMappingURL=auth.router.js.map