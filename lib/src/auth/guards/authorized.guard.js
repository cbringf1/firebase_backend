"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const http_status_enum_1 = require("../../core/enums/http-status.enum");
const session_1 = require("../models/session");
async function authorizedGuard(req, res, next) {
    if (!req.headers.authorization) {
        return next({
            status: http_status_enum_1.HttpStatus.UNAUTHORIZED,
            error: 'Unautorized',
            message: 'Access denied. No token provided',
        });
    }
    const token = req.headers.authorization.replace('Bearer ', '');
    try {
        const decodedToken = await admin.auth().verifyIdToken(token);
        req.session = new session_1.Session(decodedToken);
        return next();
    }
    catch (error) {
        return next({
            status: http_status_enum_1.HttpStatus.UNAUTHORIZED,
            error: 'Unautorized',
            message: 'Invalid token',
        });
    }
}
exports.authorizedGuard = authorizedGuard;
//# sourceMappingURL=authorized.guard.js.map