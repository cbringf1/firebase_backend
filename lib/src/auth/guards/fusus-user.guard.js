"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_enum_1 = require("../../core/enums/http-status.enum");
function fususUserGuard(req, res, next) {
    if (req.session.isFususUser) {
        return next();
    }
    return next({
        status: http_status_enum_1.HttpStatus.FORBIDDEN,
        error: 'Forbidden',
        message: 'Access denied. Only for Fusus users',
    });
}
exports.fususUserGuard = fususUserGuard;
//# sourceMappingURL=fusus-user.guard.js.map