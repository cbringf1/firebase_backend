"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_enum_1 = require("../../core/enums/http-status.enum");
const prepare_document_data_1 = require("../../core/helpers/prepare-document-data");
const users_db_1 = require("../db/users.db");
class UsersService {
    static async create(id, data) {
        try {
            await users_db_1.USERS.doc(id).set(data);
            const userDoc = await users_db_1.USERS.doc(id).get();
            return prepare_document_data_1.prepareDocumentData(userDoc);
        }
        catch (e) {
            throw {
                status: http_status_enum_1.HttpStatus.CONFLICT,
                error: 'Conflict',
                message: 'Could not create user',
            };
        }
    }
    static async get(id) {
        const userDoc = await users_db_1.USERS.doc(id).get();
        if (!userDoc.exists) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `User with id ${id} not found`,
            };
        }
        return prepare_document_data_1.prepareDocumentData(userDoc);
    }
    static async getByPhone(phone) {
        const userDocs = await users_db_1.USERS
            .where('phoneNumber', '==', phone)
            .get();
        if (userDocs.empty) {
            throw {
                status: http_status_enum_1.HttpStatus.NOT_FOUND,
                error: 'Not Found',
                message: `User with phone ${phone} not found`,
            };
        }
        return prepare_document_data_1.prepareDocumentData(userDocs.docs[0]);
    }
    static async delete(id) {
        await users_db_1.USERS.doc(id).delete();
    }
}
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map