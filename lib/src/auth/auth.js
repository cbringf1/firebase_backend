"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const TOKEN_START = 'Bearer ';
exports.validateFirebaseToken = async (req, res, next) => {
    if (!req.headers.authorization || !req.headers.authorization.startsWith(TOKEN_START)) {
        sendUnauthorized(res);
    }
    else {
        const token = req.headers.authorization.split(TOKEN_START)[1];
        const decodedToken = await admin.auth().verifyIdToken(token);
        if (decodedToken.organization_id) {
            const org = await verifyOrganization(decodedToken.organization_id);
            if (org.docs.length > 0) {
                console.log('Authentication success');
                next(req, res);
            }
            else {
                sendUnauthorized(res);
            }
        }
        else {
            sendUnauthorized(res);
        }
    }
};
function sendUnauthorized(res) {
    res.status(401).send('Unauthorized');
}
function verifyOrganization(organizationId) {
    return admin.firestore()
        .collection('organization')
        .where('id', '==', organizationId)
        .get();
}
//# sourceMappingURL=auth.js.map