"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_enum_1 = require("../../core/enums/http-status.enum");
function currentUserAction(req, res) {
    res.status(http_status_enum_1.HttpStatus.OK).send(req.session);
}
exports.currentUserAction = currentUserAction;
//# sourceMappingURL=current-user.action.js.map