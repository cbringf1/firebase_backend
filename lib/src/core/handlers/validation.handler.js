"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const http_status_enum_1 = require("../enums/http-status.enum");
function validationHandler(req, res, next) {
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        res
            .status(http_status_enum_1.HttpStatus.UNPROCESSABLE_ENTITY)
            .json({
            errors: errors.mapped(),
        });
        return;
    }
    next();
}
exports.validationHandler = validationHandler;
//# sourceMappingURL=validation.handler.js.map