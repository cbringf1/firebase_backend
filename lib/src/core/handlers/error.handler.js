"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_enum_1 = require("../enums/http-status.enum");
function errorHandler(err, req, res, next) {
    console.error(err);
    res
        .status(err.status || http_status_enum_1.HttpStatus.INTERNAL_SERVER_ERROR)
        .send(err)
        .end();
}
exports.errorHandler = errorHandler;
//# sourceMappingURL=error.handler.js.map