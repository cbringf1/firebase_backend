"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
function prepareDocumentData(docSnapshot) {
    const data = docSnapshot.data();
    Object.keys(data).forEach((key) => {
        const value = data[key];
        if (value instanceof admin.firestore.Timestamp) {
            data[key] = value.toDate();
        }
    });
    return Object.assign({ id: docSnapshot.id }, data);
}
exports.prepareDocumentData = prepareDocumentData;
//# sourceMappingURL=prepare-document-data.js.map