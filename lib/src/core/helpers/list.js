"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prepare_document_data_1 = require("./prepare-document-data");
async function list(collection, options) {
    options = Object.assign({ page: 1, pageSize: 20, ordering: undefined }, options);
    if (options.ordering) {
        const isDescDirection = options.ordering.startsWith('-');
        const orderBy = isDescDirection ? options.ordering.slice(1) : options.ordering;
        const orderByDirection = isDescDirection ? 'desc' : 'asc';
        collection = collection.orderBy(orderBy, orderByDirection);
    }
    const resultSnapshot = await collection.get();
    const results = resultSnapshot.docs.map(prepare_document_data_1.prepareDocumentData);
    return Promise.resolve({
        count: resultSnapshot.size,
        totalPages: 1,
        results,
    });
    // if (options.page < 1) {
    //   throw new Error('Invalid page');
    // }
    //
    // const startAt = (options.page - 1) * options.pageSize;
    //
    // const allResultSnapshot = await collection.get();
    // const totalPages = Math.ceil(allResultSnapshot.size / options.pageSize);
    //
    // if (options.page > totalPages) {
    //   throw new Error('Invalid page');
    // }
    //
    // const resultSnapshot = await collection
    //   .orderBy(options.ordering)
    //   .startAt(startAt)
    //   .limit(options.pageSize)
    //   .get();
    //
    // console.log(resultSnapshot.docs);
    // const results = resultSnapshot.docs.map((snapshot) => snapshot.data());
    //
    // return Promise.resolve({
    //   count: resultSnapshot.size,
    //   totalPages,
    //   results,
    // });
}
exports.list = list;
//# sourceMappingURL=list.js.map