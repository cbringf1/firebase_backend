"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function normalizePhoneSanitizer(value) {
    return value
        ? value.replace(/\s/g, '')
        : '';
}
exports.normalizePhoneSanitizer = normalizePhoneSanitizer;
//# sourceMappingURL=normalize-phone.sanitizer.js.map