"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
exports.checkUserPhone = (phoneNumber, orgId) => {
    const db = admin.firestore();
    const phone = phoneNumber.replace(/\s/g, '');
    return rxjs_1.forkJoin([
        rxjs_1.from(db.collection('user').where('phoneNumber', '==', phone).get()),
        rxjs_1.from(db.collection('invited_user').where('phoneNumber', '==', phone).get()),
    ])
        .pipe(operators_1.map(res => res.filter(d => d.docs.length > 0)), operators_1.switchMap(res => rxjs_1.of(res.length > 0 ? res[0].docs[0] : null)), operators_1.map(res => res ? Object.assign({}, res.data(), { id: res.id }) : res), operators_1.mergeMap(res => {
        return res ? rxjs_1.forkJoin([
            rxjs_1.of(res),
            rxjs_1.from(db
                .collection('organization_user')
                .where('userId', '==', res.id)
                .where('organizationId', '==', orgId).get())
                .pipe(operators_1.map(orgRes => orgRes.docs.length > 0 ? orgRes.docs[0].data() : null))
        ]) : rxjs_1.of(res);
    }), operators_1.map(res => res ? ({
        user: res[0],
        organization: res[1]
    }) : res));
};
//# sourceMappingURL=check-user-phone.function.js.map