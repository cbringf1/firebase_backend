"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sms_notification_1 = require("./sms-notification");
const config_1 = require("./config");
exports.notify = (user, messageKey) => {
    const sms = new sms_notification_1.SMSNotification();
    return sms.send(user.phoneNumber, `${user.organization.name} ${config_1.smsTemplates[messageKey]}`);
};
//# sourceMappingURL=notify.js.map