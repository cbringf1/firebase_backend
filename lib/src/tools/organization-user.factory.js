"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildOrganizationUser = (userDocId, invitedUser, newUser = false) => {
    return {
        organizationId: invitedUser.organization.id,
        organizationName: invitedUser.organization.name,
        userId: userDocId,
        userName: `${invitedUser.firstName} ${invitedUser.lastName}`,
        status: 'pending user',
        role: 'admin',
        invitedUser: newUser,
        userPhoneNumber: invitedUser.phoneNumber.replace(/\s/g, '')
    };
};
//# sourceMappingURL=organization-user.factory.js.map