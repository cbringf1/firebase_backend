"use strict";
// export const Config = {
//     apiUrl: 'https://fieldwatch-api.dev.securonetservices.com/api/service/alarms/',
//     authToken: 'Token 3cb6f5e194c16a95fc94056cd75fecf40a00376b',
//     type: 'Panic Button',
//     alias: 'firebase id',
// };
Object.defineProperty(exports, "__esModule", { value: true });
exports.FunctionErrors = {
    emptyAlarm: {
        title: 'Alarm error:',
        msg: 'Alarm data is empty or undefined'
    },
    emptyUser: {
        title: 'User creation error:',
        msg: 'Invited User data is empty or undefined'
    },
    userAlarmEmpty: {
        title: 'Alarm error:',
        msg: 'User data is empty or undefined'
    }
};
//# sourceMappingURL=config.js.map