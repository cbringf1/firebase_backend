import { IEnvironment } from "./environment.interface";

export const environment: IEnvironment = {
	apiUrl:
		"X",
	authToken: "X",
	type: "X",
	message: "Panic App alert detected",
	source: {
		object_id: "X",
		object_type: "Camera"
	},
	recording_url:
		"X.m3u8"
};
