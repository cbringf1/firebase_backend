export interface IEnvironment {
	apiUrl: string;
	authToken: string;
	type: string;
	recording_url?: string;
	source?: any;
	message: string;
}
