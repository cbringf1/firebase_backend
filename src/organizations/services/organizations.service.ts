import { firestore } from "firebase-admin";

import { HttpStatus } from "../../core/enums/http-status.enum";
import { prepareDocumentData } from "../../core/helpers/prepare-document-data";

import { ORGANIZATIONS } from "../db/organizations.db";

export class OrganizationsService {
	public static async get(id: string): Promise<firestore.DocumentData> {
		const organizationDocs = await ORGANIZATIONS.where("X", "==", id).get();

		if (organizationDocs.empty) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `Organization with id ${id} not found`
			};
		}

		return prepareDocumentData(organizationDocs.docs[0]);
	}
}
