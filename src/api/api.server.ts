import * as express from "express";
import * as cors from "cors";

import { errorHandler } from "../core/handlers/error.handler";
import { ApiRouter } from "./api.router";

const server = express();

server.use(
	cors({
		origin: true,
		optionsSuccessStatus: 200
	})
);

server.use(express.json());

server.use("/api", ApiRouter());
server.use(errorHandler);

export const ApiServer = server;
