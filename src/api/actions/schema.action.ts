import * as express from "express";

export function schemaAction(router: express.Router) {
	const routes = router.stack.reduce((acc, stack) => {
		if (stack.route) {
			acc.push(stack.route);
		} else if (stack.name === "router") {
			stack.handle.stack
				.filter((handler: any) => !!handler.route)
				.forEach((handler: any) => {
					acc.push(handler);
				});
		}

		return acc;
	}, []);

	return (req: express.Request, res: express.Response) => {
		res.status(200).send(routes);
	};
}
