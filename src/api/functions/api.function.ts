import * as functions from "firebase-functions";

import { ApiServer } from "../api.server";

export const api = functions.https.onRequest(ApiServer);
