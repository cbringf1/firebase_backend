import * as express from "express";

import { AuthRouter } from "../auth/auth.router";
import { AlertUsersRouter } from "../alert-users/alert-users.router";

import { schemaAction } from "./actions/schema.action";

export function ApiRouter() {
	const router = express.Router();

	AuthRouter(router);
	AlertUsersRouter(router);

	router.get("/schema", schemaAction(router));

	return router;
}
