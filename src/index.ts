import * as admin from 'firebase-admin';

admin.initializeApp();

export * from './api/api.functions';
export * from './auth/auth.functions';
export * from './alert-users/alert-users.functions';
export * from './schedule';
export * from './alarm';
export * from './store';
