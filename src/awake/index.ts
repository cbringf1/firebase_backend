// Functions to keep warm firebase instances

import { awakeCollectionsTriggers } from "./collection-triggers";

export * from "./filters";

export const awake = async () => {
	await awakeCollectionsTriggers();
	return true;
};
