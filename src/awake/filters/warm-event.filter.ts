import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

const warmDocKey = "_warming";

export const warmEventFilter = (
	fn: Function,
	target:
		| functions.Change<admin.firestore.DocumentSnapshot>
		| admin.firestore.DocumentSnapshot,
	context: functions.EventContext
) => {
	let data = null;

	if (target instanceof admin.firestore.DocumentSnapshot) {
		data = target.data();
	} else {
		data = target.after.data();
	}

	return data && !(warmDocKey in data)
		? fn(target, context)
		: Promise.resolve("warming");
};
