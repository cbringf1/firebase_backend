import * as admin from "firebase-admin";

const collections = ["alarm", "invited_user"];
const warmDocKey = "_warming";

export const awakeCollectionsTriggers = async () => {
	for (const collection of collections) {
		const docRef = await admin
			.firestore()
			.collection(collection)
			.add({
				[warmDocKey]: true
			});

		await docRef.update({
			[warmDocKey]: false
		});
		await docRef.delete();
	}
};
