export const FunctionErrors = {
	emptyAlarm: {
		title: "Alarm error:",
		msg: "Alarm data is empty or undefined"
	},
	emptyUser: {
		title: "User creation error:",
		msg: "Invited User data is empty or undefined"
	},
	userAlarmEmpty: {
		title: "Alarm error:",
		msg: "User data is empty or undefined"
	}
};
