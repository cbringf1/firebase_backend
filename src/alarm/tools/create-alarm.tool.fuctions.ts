import { ORGANIZATION_USER } from "../db/alert-user-invites.db";
import { prepareDocumentData } from "../../core/helpers/prepare-document-data";
import { HttpStatus } from "../../core/enums/http-status.enum";

export const getAlarmExtraInfo = async (orgId: any, userId: any) => {
	const organizationUserDoc = await ORGANIZATION_USER.where(
		"X",
		"==",
		orgId
	)
		.where("X", "==", userId)
		.get();

	if (organizationUserDoc.empty) {
		throw {
			status: HttpStatus.NOT_FOUND,
			error: "Not Found",
			message: `Organization for user with the Id: ${userId} not found`
		};
	}

	return prepareDocumentData(organizationUserDoc.docs[0]);
};
