import { XMLHttpRequest } from "xmlhttprequest-ts";

export const onCreateAlarmApiCall = (
	jsonData: string,
	apiUrl: string,
	auth: string
) => {
	const http = new XMLHttpRequest();

	http.onreadystatechange = () => {
		console.log("ResponseText:", http.responseText);
	};
	http.open("POST", apiUrl);

	http.setRequestHeader("Authorization", auth);
	http.setRequestHeader("Content-Type", "application/json");
	http.setRequestHeader("Accept", "application/json");

	http.send(jsonData);
};
