export interface IAlarm {
	geometry: any;
	type: string;
	alias: string;
	details: any;
	organization: string;
	recording_url?: string;
	source?: any;
	message: string;
}
