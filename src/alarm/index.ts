// Cloud functions related to alarm creation

import * as functions from "firebase-functions";

import { warmEventFilter } from "../awake";
import { onCreateAlarm } from "./create-alarm.function";

export const onCreateAlarmTrigger = functions.firestore
	.document("X/{userId}")
	.onCreate((snap, context) => warmEventFilter(onCreateAlarm, snap, context));
