import * as functions from "firebase-functions";
import * as uuid from "uuid";

import { environment } from "../../environments/environment";
import { IAlarm } from "./models/alarm";
import { onCreateAlarmApiCall } from "./tools/create-alarm.api";
import { FunctionErrors } from "../config";
import { UsersService } from "../auth/services/users.service";
import { getAlarmExtraInfo } from "./tools/create-alarm.tool.fuctions";

export const onCreateAlarm = async (
	snap: functions.firestore.DocumentSnapshot,
	context: functions.EventContext
) => {
	const alarm = snap.data();

	if (alarm) {
		let onCreateAlarmData: IAlarm = {
			type: environment.type,
			alias: uuid().slice(0, 8),
			geometry: {
				type: "Point",
				coordinates: [alarm.longitude, alarm.latitude]
			},
			organization: alarm.organizationId,
			details: {
				"User Name": alarm.user.firstName + " " + alarm.user.lastName,
				"User Phone Number": alarm.user.phoneNumber
			},
			message: environment.message
		};
		const userDoc = await UsersService.getByPhone(
			alarm.user.phoneNumber
		).catch(() => undefined);
		const userId = userDoc ? userDoc.id : null;

		if (userId && alarm.organizationId) {
			const organizationUserDoc = await getAlarmExtraInfo(
				alarm.organizationId,
				userId
			);

			if (organizationUserDoc) {
				onCreateAlarmData = {
					...onCreateAlarmData,
					...(organizationUserDoc.recording_url && {
						recording_url: organizationUserDoc.recording_url
					}),
					...(organizationUserDoc.source && {
						source: {
							object_id: organizationUserDoc.source.object_id,
							object_type: organizationUserDoc.source.object_type
						}
					})
				};
				const alarmData = JSON.stringify(onCreateAlarmData);

				onCreateAlarmApiCall(
					alarmData,
					environment.apiUrl,
					environment.authToken
				);
				return "OK";
			}
		} else {
			console.log(
				FunctionErrors.emptyAlarm.title,
				FunctionErrors.emptyAlarm.msg
			);
			console.log("Context:", context);
		}
	}
	return "Fail";
};
