// Scheduled functions

import * as functions from "firebase-functions";

import { awake } from "../awake";

export const scheduledFunction = functions.pubsub
	.schedule("every 5 minutes")
	.onRun(() => awake());
