import * as admin from "firebase-admin";

import { prepareDocumentData } from "./prepare-document-data";

export interface IListOptions {
	page?: number;
	pageSize?: number;
	ordering?: string;
}

export interface IListResponse {
	count: number;
	totalPages: number;
	results: admin.firestore.DocumentData[];
}

export async function list(
	collection: admin.firestore.CollectionReference | admin.firestore.Query,
	options?: IListOptions
): Promise<IListResponse> {
	options = {
		page: 1,
		pageSize: 20,
		ordering: undefined,
		...options
	};

	if (options.ordering) {
		const isDescDirection = options.ordering.startsWith("-");
		const orderBy = isDescDirection
			? options.ordering.slice(1)
			: options.ordering;
		const orderByDirection = isDescDirection ? "desc" : "asc";

		collection = collection.orderBy(orderBy, orderByDirection);
	}

	const resultSnapshot = await collection.get();
	const results = resultSnapshot.docs.map(prepareDocumentData);

	return Promise.resolve({
		count: resultSnapshot.size,
		totalPages: 1,
		results
	});
}
