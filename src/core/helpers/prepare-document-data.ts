import * as admin from "firebase-admin";

export function prepareDocumentData(
	docSnapshot:
		| admin.firestore.DocumentSnapshot
		| admin.firestore.QueryDocumentSnapshot
): admin.firestore.DocumentData {
	const data = docSnapshot.data()!;

	Object.keys(data).forEach(key => {
		const value = data[key];

		if (value instanceof admin.firestore.Timestamp) {
			data[key] = value.toDate();
		}
	});
	return {
		id: docSnapshot.id,
		...data
	};
}
