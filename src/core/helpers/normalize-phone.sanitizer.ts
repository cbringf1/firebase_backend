export function normalizePhoneSanitizer(value: string) {
	return value ? value.replace(/\s/g, "") : "";
}
