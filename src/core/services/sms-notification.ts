import * as functions from "firebase-functions";

import * as AWS from "aws-sdk";

import * as _ from "lodash";

class SMSNotification {
	private readonly awsSNS: AWS.SNS;

	constructor() {
		AWS.config.credentials = _.merge(
			AWS.config.credentials,
			_.omit(
				_.mapKeys(functions.config().aws_sms, (v, k) => _.camelCase(k)),
				"region"
			)
		);
		AWS.config.update(_.pick(functions.config().aws_sms, ["region"]));

		this.awsSNS = new AWS.SNS({ apiVersion: "2010-03-31" });
	}

	send(phoneNumber: string, message: string) {
		return this.awsSNS
			.publish({
				PhoneNumber: phoneNumber,
				Message: message
			})
			.promise();
	}
}

export const SMS = new SMSNotification();
