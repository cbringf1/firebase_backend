import * as express from "express";
import { validationResult } from "express-validator";

import { HttpStatus } from "../enums/http-status.enum";

export function validationHandler(
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
) {
	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
			errors: errors.mapped()
		});
		return;
	}
	next();
}
