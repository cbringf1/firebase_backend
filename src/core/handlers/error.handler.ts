import * as express from "express";

import { HttpStatus } from "../enums/http-status.enum";

export function errorHandler(
	err: { status: HttpStatus; error: string; message: string },
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
): void {
	console.error(err);

	res.status(err.status || HttpStatus.INTERNAL_SERVER_ERROR)
		.send(err)
		.end();
}
