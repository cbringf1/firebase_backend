export interface IUser {
	firstName: string;
	lastName: string;
	email?: string;
	phoneNumber: string;
	isActive: boolean;
	call911: boolean;
}
