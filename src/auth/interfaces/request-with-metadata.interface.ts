import * as express from "express";

import { Session } from "../models/session";

export interface AuthRequest extends express.Request {
	session?: Session;
}
