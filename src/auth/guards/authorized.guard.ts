import * as admin from "firebase-admin";
import * as express from "express";

import { HttpStatus } from "../../core/enums/http-status.enum";

import { AuthRequest } from "../interfaces/request-with-metadata.interface";
import { Session } from "../models/session";

export async function authorizedGuard(
	req: AuthRequest,
	res: express.Response,
	next: express.NextFunction
): Promise<void> {
	if (!req.headers.authorization) {
		return next({
			status: HttpStatus.UNAUTHORIZED,
			error: "Unautorized",
			message: "Access denied. No token provided"
		});
	}

	const token = req.headers.authorization.replace("Bearer ", "");

	try {
		const decodedToken = await admin.auth().verifyIdToken(token);

		req.session = new Session(decodedToken);
		return next();
	} catch (error) {
		return next({
			status: HttpStatus.UNAUTHORIZED,
			error: "Unautorized",
			message: "Invalid token"
		});
	}
}
