import * as express from "express";

import { AuthRequest } from "../interfaces/request-with-metadata.interface";
import { HttpStatus } from "../../core/enums/http-status.enum";

export function fususUserGuard(
	req: AuthRequest,
	res: express.Response,
	next: express.NextFunction
): void {
	if (req.session!.isFususUser) {
		return next();
	}

	return next({
		status: HttpStatus.FORBIDDEN,
		error: "Forbidden",
		message: "Access denied. Only for Fusus users"
	});
}
