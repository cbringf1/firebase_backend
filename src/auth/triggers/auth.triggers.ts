import * as functions from "firebase-functions";

import { UsersService } from "../services/users.service";

export const onAuthUserCreate = functions.auth.user().onCreate(async user => {
	await UsersService.create(user.uid, {
		email: user.email,
		phoneNumber: user.phoneNumber
	});
});

export const onAuthUserDelete = functions.auth.user().onDelete(async user => {
	await UsersService.delete(user.uid);
});
