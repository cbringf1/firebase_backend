import * as express from "express";

import { authorizedGuard } from "./guards/authorized.guard";
import { currentUserAction } from "./actions/current-user.action";

const router = express.Router();

export function AuthRouter(appRouter: express.Router) {
	appRouter.use(router);

	router.get("/current-user", authorizedGuard, currentUserAction);
}
