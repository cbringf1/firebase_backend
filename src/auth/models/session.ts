import * as admin from "firebase-admin";

export class Session {
	public readonly userId: string;
	public readonly organizationId?: string;

	public readonly isFususUser: boolean;
	public readonly isFirebaseUser: boolean;

	public readonly provider: string;

	constructor(decodedToken: admin.auth.DecodedIdToken) {
		this.userId = decodedToken.sub;
		this.organizationId = decodedToken.organization_id;

		this.provider = decodedToken.firebase.sign_in_provider;

		this.isFususUser = this.provider === "custom" && !!this.organizationId;
		this.isFirebaseUser = !this.isFususUser;
	}
}
