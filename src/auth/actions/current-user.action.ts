import * as express from "express";

import { HttpStatus } from "../../core/enums/http-status.enum";

import { AuthRequest } from "../interfaces/request-with-metadata.interface";

export function currentUserAction(req: AuthRequest, res: express.Response) {
	res.status(HttpStatus.OK).send(req.session);
}
