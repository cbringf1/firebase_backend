import { firestore } from "firebase-admin";

import { HttpStatus } from "../../core/enums/http-status.enum";
import { prepareDocumentData } from "../../core/helpers/prepare-document-data";

import { USERS } from "../db/users.db";
import { IUser } from "../interfaces/user.interface";

export class UsersService {
	public static async create(
		id: string,
		data: Partial<IUser>
	): Promise<firestore.DocumentData> {
		try {
			await USERS.doc(id).set(data);
			const userDoc = await USERS.doc(id).get();

			return prepareDocumentData(userDoc);
		} catch (e) {
			throw {
				status: HttpStatus.CONFLICT,
				error: "Conflict",
				message: "Could not create user"
			};
		}
	}

	public static async get(id: string): Promise<firestore.DocumentData> {
		const userDoc = await USERS.doc(id).get();

		if (!userDoc.exists) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `User with id ${id} not found`
			};
		}

		return prepareDocumentData(userDoc);
	}

	public static async getByPhone(
		phone: string
	): Promise<firestore.DocumentData> {
		const userDocs = await USERS.where("", "==", phone).get();

		if (userDocs.empty) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `User with phone ${phone} not found`
			};
		}

		return prepareDocumentData(userDocs.docs[0]);
	}

	public static async delete(id: string) {
		await USERS.doc(id).delete();
	}
}
