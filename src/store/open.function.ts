import * as functions from "firebase-functions";

const PLAY_STORE =
	"https://play.google.com/store/apps/X";
const APP_STORE = "https://apps.apple.com/us/app/X";
const FUSUS = "https://X";

export const onOpenStore = (
	req: functions.Request,
	res: functions.Response
) => {
	const userAgent = req.headers["user-agent"];

	if (/android/i.test(userAgent!)) {
		res.redirect(PLAY_STORE);
	} else if (/iPad|iPhone|iPod/.test(userAgent!)) {
		res.redirect(APP_STORE);
	} else {
		res.redirect(FUSUS);
	}
};
