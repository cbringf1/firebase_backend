// Functions to redirect users to correct app store [Android, IOS]

import * as functions from "firebase-functions";

import { onOpenStore } from "./open.function";

export const openStore = functions.https.onRequest(onOpenStore);
