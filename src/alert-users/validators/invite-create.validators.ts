import { body, sanitizeBody, Meta } from "express-validator";

import { normalizePhoneSanitizer } from "../../core/helpers/normalize-phone.sanitizer";

import { ALERT_USER_INVITES } from "../db/alert-user-invites.db";

async function phoneNumberIsUnique(value: string, { req }: Meta) {
	const organizationId = req.session!.organizationId;
	const inviteIsExisted = await ALERT_USER_INVITES.where(
		"X",
		"==",
		organizationId
	)
		.where("X", "==", value)
		.get();

	if (!inviteIsExisted.empty) {
		throw new Error("Phone number already exists");
	}

	return inviteIsExisted.empty;
}

export const inviteCreateValidators = [
	sanitizeBody("X").customSanitizer(normalizePhoneSanitizer),

	body("X")
		.exists()
		.withMessage("Field is required"),

	body("X")
		.exists()
		.withMessage("Field is required"),

	body("X")
		.exists()
		.withMessage("Field is required")
		.custom(phoneNumberIsUnique)
		.withMessage("Phone number already exists")
];
