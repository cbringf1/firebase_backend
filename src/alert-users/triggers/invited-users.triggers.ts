import * as functions from "firebase-functions";

import { ALERT_USER_INVITES } from "../db/alert-user-invites.db";
import { AlertUserInvitesService } from "../services/alert-user-invites.service";

export const onInvitedUserUpdate = functions.firestore
	.document("X/{id}")
	.onUpdate(async change => {
		const snapshot = change.after;
		const data = snapshot.data();

		const userId = data && data.id;

		if (userId) {
			const userInvites = await ALERT_USER_INVITES.where(
				"X",
				"==",
				snapshot.id
			).get();

			for (const invite of userInvites.docs) {
				await AlertUserInvitesService.update(invite.id, { userId });
			}
		}
	});
