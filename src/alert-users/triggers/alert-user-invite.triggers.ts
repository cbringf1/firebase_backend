import * as functions from "firebase-functions";

export const onAlertUserInviteCreate = functions.firestore
	.document("X/{id}")
	.onCreate(snap => {
		return snap.ref.update({
			createdAt: snap.createTime
		});
	});
