import * as express from "express";

import { list } from "../../core/helpers/list";
import { HttpStatus } from "../../core/enums/http-status.enum";
import { AuthRequest } from "../../auth/interfaces/request-with-metadata.interface";

import { ALERT_USER_INVITES } from "../db/alert-user-invites.db";
import { AlertUserIntiveStatus } from "../enums/alert-user-invite-status.enum";
import { AlertUserInvitesService } from "../services/alert-user-invites.service";

export class AlertUserInvitesController {
	public static async list(
		req: AuthRequest,
		res: express.Response,
		next: express.NextFunction
	) {
		const session = req.session!;

		const collection = await ALERT_USER_INVITES.where(
			"X",
			"==",
			session.organizationId
		);

		const response = await list(collection, req.query);

		return res.status(200).send(response);
	}

	public static async create(
		req: AuthRequest,
		res: express.Response,
		next: express.NextFunction
	) {
		const session = req.session!;

		try {
			const invite = await AlertUserInvitesService.create(
				req.body,
				session.organizationId!
			);

			return res.status(HttpStatus.CREATED).send(invite);
		} catch (e) {
			return next(e);
		}
	}

	public static async activate(
		req: express.Request,
		res: express.Response,
		next: express.NextFunction
	) {
		try {
			const invite = await AlertUserInvitesService.update(req.params.id, {
				organizationStatus: AlertUserIntiveStatus.Accepted
			});

			return res.status(HttpStatus.OK).send(invite);
		} catch (e) {
			return next(e);
		}
	}

	public static async delete(
		req: express.Request,
		res: express.Response,
		next: express.NextFunction
	) {
		try {
			await AlertUserInvitesService.remove(req.params.id);

			return res.status(HttpStatus.OK).send();
		} catch (e) {
			return next(e);
		}
	}

	public static async deactivate(
		req: express.Request,
		res: express.Response,
		next: express.NextFunction
	) {
		try {
			const invite = await AlertUserInvitesService.update(req.params.id, {
				organizationStatus: AlertUserIntiveStatus.Rejected
			});

			return res.status(HttpStatus.OK).send(invite);
		} catch (e) {
			return next(e);
		}
	}
}
