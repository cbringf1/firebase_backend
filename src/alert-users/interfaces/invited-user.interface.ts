export interface IInvitedUser {
	firstName: string;
	lastName: string;
	phoneNumber: string;
}
