import { AlertUserIntiveStatus } from "../enums/alert-user-invite-status.enum";

export interface IAlertUserInvite {
	userId: string;
	userName: string;
	userPhoneNumber: string;
	organizationId: string;
	organizationName: string;
	userStatus: AlertUserIntiveStatus;
	organizationStatus: AlertUserIntiveStatus;
}
