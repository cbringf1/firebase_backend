import * as express from "express";

import { fususUserGuard } from "../auth/guards/fusus-user.guard";
import { authorizedGuard } from "../auth/guards/authorized.guard";
import { validationHandler } from "../core/handlers/validation.handler";

import { inviteCreateValidators } from "./validators/invite-create.validators";
import { AlertUserInvitesController } from "./controllers/alert-user-invites.controller";

const router = express.Router();

export function AlertUsersRouter(appRouter: express.Router) {
	appRouter.use("/alert-users", router);

	router.use(authorizedGuard, fususUserGuard);

	router.get("/invites", AlertUserInvitesController.list);
	router.post(
		"/invites",
		inviteCreateValidators,
		validationHandler,
		AlertUserInvitesController.create
	);

	router.patch("/invites/:id/activate", AlertUserInvitesController.activate);
	router.patch(
		"/invites/:id/deactivate",
		AlertUserInvitesController.deactivate
	);
	router.patch("/invites/:id/delete", AlertUserInvitesController.delete);
}
