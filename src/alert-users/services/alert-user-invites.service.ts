import { firestore } from "firebase-admin";

import { SMS } from "../../core/services/sms-notification";
import { HttpStatus } from "../../core/enums/http-status.enum";
import { UsersService } from "../../auth/services/users.service";
import { prepareDocumentData } from "../../core/helpers/prepare-document-data";
import { OrganizationsService } from "../../organizations/services/organizations.service";

import { IInvitedUser } from "../interfaces/invited-user.interface";
import { IAlertUserInvite } from "../interfaces/alert-user-invite.interface";
import { ALERT_USER_INVITES } from "../db/alert-user-invites.db";
import { AlertUserIntiveStatus } from "../enums/alert-user-invite-status.enum";
import { InvitedUsersService } from "./invited-users.service";

export class AlertUserInvitesService {
	public static async create(userData: IInvitedUser, organizationId: string) {
		const user = await UsersService.getByPhone(userData.phoneNumber).catch(
			() => undefined
		);
		const organization = await OrganizationsService.get(
			organizationId
		).catch(error => {
			throw {
				status: HttpStatus.BAD_REQUEST,
				error: "Bad Request",
				message: error.message
			};
		});

		try {
			const inviteUser = await InvitedUsersService.getByPhone(
				userData.phoneNumber
			).catch(() => InvitedUsersService.create(userData));
			const inviteData = {
				userId: user ? user.id : inviteUser.id,
				userName: `${userData.firstName} ${userData.lastName}`,
				userPhoneNumber: userData.phoneNumber,
				organizationId,
				organizationName: organization.name,
				userStatus: AlertUserIntiveStatus.Pending,
				organizationStatus: AlertUserIntiveStatus.Accepted
			};
			const inviteRef = await ALERT_USER_INVITES.add(inviteData);
			const inviteDoc = await inviteRef.get();

			await SMS.send(
				userData.phoneNumber,
				`${organization.name} have invited you to join the organization in fusus Panic App. X`
			);

			return prepareDocumentData(inviteDoc);
		} catch (error) {
			throw {
				status: HttpStatus.CONFLICT,
				error: "Conflict",
				message: "Could not create alert user invite"
			};
		}
	}

	public static async get(id: string): Promise<firestore.DocumentData> {
		const inviteDoc = await ALERT_USER_INVITES.doc(id).get();

		if (!inviteDoc.exists) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `Alert user invite with id ${id} not found`
			};
		}

		return prepareDocumentData(inviteDoc);
	}

	public static async update(
		id: string,
		data: Partial<IAlertUserInvite>
	): Promise<firestore.DocumentData> {
		const inviteDoc = await ALERT_USER_INVITES.doc(id).get();

		if (!inviteDoc.exists) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `Alert user invite with id ${id} not found`
			};
		}

		try {
			await inviteDoc.ref.update(data);
			return await AlertUserInvitesService.get(id);
		} catch (e) {
			throw {
				status: HttpStatus.CONFLICT,
				error: "Conflict",
				message: "Could not update alert user invite"
			};
		}
	}

	public static async remove(id: string) {
		try {
			await ALERT_USER_INVITES.doc(id).delete();
		} catch (e) {
			throw {
				status: HttpStatus.CONFLICT,
				error: "Conflict",
				message: "Could not remove organization_user entry"
			};
		}
	}
}
