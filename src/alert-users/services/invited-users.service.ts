import { firestore } from "firebase-admin";

import { HttpStatus } from "../../core/enums/http-status.enum";
import { prepareDocumentData } from "../../core/helpers/prepare-document-data";

import { IInvitedUser } from "../interfaces/invited-user.interface";
import { INVITED_USERS } from "../db/invited-users.db";

export class InvitedUsersService {
	public static async create(
		data: IInvitedUser
	): Promise<firestore.DocumentData> {
		try {
			const userRef = await INVITED_USERS.add(data);
			const userDoc = await userRef.get();

			return prepareDocumentData(userDoc);
		} catch (e) {
			throw {
				status: HttpStatus.CONFLICT,
				error: "Conflict",
				message: "Could not create invited user"
			};
		}
	}

	public static async get(id: string): Promise<firestore.DocumentData> {
		const userDoc = await INVITED_USERS.doc(id).get();

		if (!userDoc.exists) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `Invited user with id ${id} not found`
			};
		}

		return prepareDocumentData(userDoc);
	}

	public static async getByPhone(
		phone: string
	): Promise<firestore.DocumentData> {
		const userDocs = await INVITED_USERS.where(
			"X",
			"==",
			phone
		).get();

		if (userDocs.empty) {
			throw {
				status: HttpStatus.NOT_FOUND,
				error: "Not Found",
				message: `Alert user with phone ${phone} not found`
			};
		}

		return prepareDocumentData(userDocs.docs[0]);
	}
}
