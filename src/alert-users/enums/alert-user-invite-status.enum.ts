export enum AlertUserIntiveStatus {
	Pending = "pending",
	Rejected = "rejected",
	Accepted = "accepted",
	Disabled = "disabled"
}
